# Tidy Elections

The aim for Tidy Elections is to provide clean, accessible election result maps and data. Its main focus is relatively little cared about elections, especially those in countries where clean election data and maps are hard to find.

You can find the project at [tidyelections.in](http://tidyelections.in).

## Contributing

Interested in contributing? Great! You can help in various ways:

- Providing election result data
- Providing svg maps of missing countries
- Adding features to the site
- Sharing the site

More details can be found on [the contribute page](https://gitlab.com/guushoekman/tidy-elections/blob/master/CONTRIBUTING.md).

## Technical details

Tidy Elections is hosted here on GitLab. It's a simple [Jekyll](https://jekyllrb.com/) site. To run it locally just clone the repo and run `bundle exec jekyll serve`.

### Let's Encrypt

`letsencrypt certonly -d tidyelections.in --manual`