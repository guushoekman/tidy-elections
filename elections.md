---
layout: base
class: elections
title: All elections
---
<div class="ui attached segment available">
	<h2>Available elections</h2>
	<table class="ui sortable unstackable table">
		<thead>
			<tr>
				<th class="eight wide">Election</th>
				<th>Country</th>
				<th class="sorted descending">Date</th>
			</tr>
		</thead>
		<tbody>
			{% assign sortedElections = site.elections | sort: 'date' | reverse %}
			{% for election in sortedElections %}
			<tr>
				<td class="selectable"><a href="{{ election.url }}">{{ election.title }}</a></td>
				<td data-sort-value="{{ election.country }}"><i class="{{ election.country_code }} flag"></i>{{ election.country }}</td>
				<td data-sort-value="{{ election.date | date: '%s' }}">{{ election.date | date: "%d %B %Y" }}</td>
			</tr>
			{% endfor %}
		</tbody>
	</table>
</div>
