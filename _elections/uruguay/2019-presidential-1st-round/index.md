---
layout: election
title: "2019 Uruguayan presidential election - 1st round"
country: Uruguay
country_code: uy
date: 2019-10-27
wikipedia: https://en.wikipedia.org/wiki/2019_Uruguayan_general_election
source: https://eleccionesnacionales.corteelectoral.gub.uy/ResumenResultados.htm
maxpercentage: 50
handicap: 2
race: both
candidates:
- name: Daniel Martínez
  name_short: FA
  party: Frente Amplio
  color: "#008000"
- name: Luis Lacalle Pou
  name_short: PN
  party: Partido Nacional
  color: "#80BFFF"
- name: Ernesto Talvi
  name_short: PC
  party: Partido Colorado
  color: "#C80815"
- name: Guido Manini Ríos
  name_short: PCA
  party: Partido Cabildo Abierto
  color: "#F4C626"
---
