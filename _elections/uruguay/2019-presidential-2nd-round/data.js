var data = [
  {
    "Area": "Uruguay",
    "FA": 1152271,
    "PN": 1189313,
    "FA_percent": 0.492,
    "PN_percent": 0.508,
    "Winner": "PN"
  },
  {
    "Area": "Artigas",
    "FA": 19800,
    "PN": 34958,
    "FA_percent": 0.362,
    "PN_percent": 0.638,
    "Winner": "PN"
  },
  {
    "Area": "Canelones",
    "FA": 189577,
    "PN": 165350,
    "FA_percent": 0.534,
    "PN_percent": 0.466,
    "Winner": "FA"
  },
  {
    "Area": "Cerro Largo",
    "FA": 26466,
    "PN": 38620,
    "FA_percent": 0.407,
    "PN_percent": 0.593,
    "Winner": "PN"
  },
  {
    "Area": "Colonia",
    "FA": 41946,
    "PN": 52111,
    "FA_percent": 0.446,
    "PN_percent": 0.554,
    "Winner": "PN"
  },
  {
    "Area": "Durazno",
    "FA": 17905,
    "PN": 26948,
    "FA_percent": 0.399,
    "PN_percent": 0.601,
    "Winner": "PN"
  },
  {
    "Area": "Flores",
    "FA": 6990,
    "PN": 13102,
    "FA_percent": 0.348,
    "PN_percent": 0.652,
    "Winner": "PN"
  },
  {
    "Area": "Florida",
    "FA": 22419,
    "PN": 29257,
    "FA_percent": 0.434,
    "PN_percent": 0.566,
    "Winner": "PN"
  },
  {
    "Area": "Lavalleja",
    "FA": 15934,
    "PN": 30249,
    "FA_percent": 0.345,
    "PN_percent": 0.655,
    "Winner": "PN"
  },
  {
    "Area": "Maldonado",
    "FA": 48768,
    "PN": 74193,
    "FA_percent": 0.397,
    "PN_percent": 0.603,
    "Winner": "PN"
  },
  {
    "Area": "Montevideo",
    "FA": 507346,
    "PN": 383991,
    "FA_percent": 0.569,
    "PN_percent": 0.431,
    "Winner": "FA"
  },
  {
    "Area": "Paysandú",
    "FA": 40114,
    "PN": 43404,
    "FA_percent": 0.48,
    "PN_percent": 0.52,
    "Winner": "PN"
  },
  {
    "Area": "Río Negro",
    "FA": 17485,
    "PN": 22953,
    "FA_percent": 0.432,
    "PN_percent": 0.568,
    "Winner": "PN"
  },
  {
    "Area": "Rivera",
    "FA": 24029,
    "PN": 52842,
    "FA_percent": 0.313,
    "PN_percent": 0.687,
    "Winner": "PN"
  },
  {
    "Area": "Rocha",
    "FA": 24129,
    "PN": 30736,
    "FA_percent": 0.44,
    "PN_percent": 0.56,
    "Winner": "PN"
  },
  {
    "Area": "Salto",
    "FA": 43398,
    "PN": 48240,
    "FA_percent": 0.474,
    "PN_percent": 0.526,
    "Winner": "PN"
  },
  {
    "Area": "San José",
    "FA": 35178,
    "PN": 40247,
    "FA_percent": 0.466,
    "PN_percent": 0.534,
    "Winner": "PN"
  },
  {
    "Area": "Soriano",
    "FA": 30453,
    "PN": 34318,
    "FA_percent": 0.47,
    "PN_percent": 0.53,
    "Winner": "PN"
  },
  {
    "Area": "Tacuarembó",
    "FA": 26179,
    "PN": 44092,
    "FA_percent": 0.373,
    "PN_percent": 0.627,
    "Winner": "PN"
  },
  {
    "Area": "Treinta y Tres",
    "FA": 14155,
    "PN": 23702,
    "FA_percent": 0.374,
    "PN_percent": 0.626,
    "Winner": "PN"
  }
]