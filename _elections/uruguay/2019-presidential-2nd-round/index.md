---
layout: election
title: "2019 Uruguayan presidential election - 2nd round"
country: Uruguay
country_code: uy
date: 2019-11-24
wikipedia: https://en.wikipedia.org/wiki/2019_Uruguayan_general_election
source: https://segundaeleccion2019.corteelectoral.gub.uy/ResumenResultados.htm
maxpercentage: 80
handicap: 1.25
race: both
candidates:
- name: Daniel Martínez
  name_short: FA
  party: Frente Amplio
  color: "#008000"
- name: Luis Lacalle Pou
  name_short: PN
  party: Partido Nacional
  color: "#80BFFF"
---