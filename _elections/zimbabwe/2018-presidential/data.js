var data = [
  {
    "Area": "Zimbabwe",
    "Chamisa": 2152955,
    "Mnangagwa": 2463210,
    "Chamisa_percent": 0.45,
    "Mnangagwa_percent": 0.515,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Beitbridge",
    "Chamisa": 16757,
    "Mnangagwa": 21091,
    "Chamisa_percent": 0.416,
    "Mnangagwa_percent": 0.524,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Bikita",
    "Chamisa": 23920,
    "Mnangagwa": 31609,
    "Chamisa_percent": 0.412,
    "Mnangagwa_percent": 0.545,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Bindura",
    "Chamisa": 25750,
    "Mnangagwa": 46562,
    "Chamisa_percent": 0.349,
    "Mnangagwa_percent": 0.632,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Binga",
    "Chamisa": 36833,
    "Mnangagwa": 14122,
    "Chamisa_percent": 0.674,
    "Mnangagwa_percent": 0.259,
    "Winner": "Chamisa"
  },
  {
    "Area": "Bubi",
    "Chamisa": 6966,
    "Mnangagwa": 16892,
    "Chamisa_percent": 0.258,
    "Mnangagwa_percent": 0.626,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Buhera",
    "Chamisa": 35617,
    "Mnangagwa": 46285,
    "Chamisa_percent": 0.418,
    "Mnangagwa_percent": 0.543,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Bulawayo",
    "Chamisa": 144107,
    "Mnangagwa": 60168,
    "Chamisa_percent": 0.669,
    "Mnangagwa_percent": 0.279,
    "Winner": "Chamisa"
  },
  {
    "Area": "Bulilima",
    "Chamisa": 10076,
    "Mnangagwa": 14761,
    "Chamisa_percent": 0.369,
    "Mnangagwa_percent": 0.54,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Chegutu",
    "Chamisa": 50946,
    "Mnangagwa": 44872,
    "Chamisa_percent": 0.517,
    "Mnangagwa_percent": 0.455,
    "Winner": "Chamisa"
  },
  {
    "Area": "Chikomba",
    "Chamisa": 18838,
    "Mnangagwa": 30101,
    "Chamisa_percent": 0.37,
    "Mnangagwa_percent": 0.592,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Chimanimani",
    "Chamisa": 17285,
    "Mnangagwa": 26776,
    "Chamisa_percent": 0.379,
    "Mnangagwa_percent": 0.588,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Chipinge",
    "Chamisa": 44826,
    "Mnangagwa": 46356,
    "Chamisa_percent": 0.467,
    "Mnangagwa_percent": 0.483,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Chiredzi",
    "Chamisa": 27702,
    "Mnangagwa": 67131,
    "Chamisa_percent": 0.282,
    "Mnangagwa_percent": 0.682,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Chirumhanzu",
    "Chamisa": 13207,
    "Mnangagwa": 29542,
    "Chamisa_percent": 0.302,
    "Mnangagwa_percent": 0.675,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Chivi",
    "Chamisa": 17055,
    "Mnangagwa": 36327,
    "Chamisa_percent": 0.307,
    "Mnangagwa_percent": 0.654,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Gokwe North",
    "Chamisa": 22118,
    "Mnangagwa": 62828,
    "Chamisa_percent": 0.251,
    "Mnangagwa_percent": 0.713,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Gokwe South",
    "Chamisa": 39183,
    "Mnangagwa": 65476,
    "Chamisa_percent": 0.359,
    "Mnangagwa_percent": 0.6,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Goromonzi",
    "Chamisa": 69931,
    "Mnangagwa": 47167,
    "Chamisa_percent": 0.585,
    "Mnangagwa_percent": 0.395,
    "Winner": "Chamisa"
  },
  {
    "Area": "Guruve",
    "Chamisa": 8265,
    "Mnangagwa": 45835,
    "Chamisa_percent": 0.149,
    "Mnangagwa_percent": 0.826,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Gutu",
    "Chamisa": 28198,
    "Mnangagwa": 43512,
    "Chamisa_percent": 0.378,
    "Mnangagwa_percent": 0.583,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Gwanda",
    "Chamisa": 24628,
    "Mnangagwa": 20308,
    "Chamisa_percent": 0.514,
    "Mnangagwa_percent": 0.424,
    "Winner": "Chamisa"
  },
  {
    "Area": "Gweru",
    "Chamisa": 67421,
    "Mnangagwa": 40351,
    "Chamisa_percent": 0.609,
    "Mnangagwa_percent": 0.364,
    "Winner": "Chamisa"
  },
  {
    "Area": "Harare",
    "Chamisa": 548889,
    "Mnangagwa": 204710,
    "Chamisa_percent": 0.717,
    "Mnangagwa_percent": 0.267,
    "Winner": "Chamisa"
  },
  {
    "Area": "Hurungwe",
    "Chamisa": 38782,
    "Mnangagwa": 67855,
    "Chamisa_percent": 0.349,
    "Mnangagwa_percent": 0.61,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Hwange",
    "Chamisa": 48908,
    "Mnangagwa": 17541,
    "Chamisa_percent": 0.703,
    "Mnangagwa_percent": 0.252,
    "Winner": "Chamisa"
  },
  {
    "Area": "Insiza",
    "Chamisa": 11776,
    "Mnangagwa": 19016,
    "Chamisa_percent": 0.357,
    "Mnangagwa_percent": 0.577,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Kariba",
    "Chamisa": 14555,
    "Mnangagwa": 11631,
    "Chamisa_percent": 0.536,
    "Mnangagwa_percent": 0.428,
    "Winner": "Chamisa"
  },
  {
    "Area": "Kwekwe",
    "Chamisa": 65697,
    "Mnangagwa": 48083,
    "Chamisa_percent": 0.558,
    "Mnangagwa_percent": 0.409,
    "Winner": "Chamisa"
  },
  {
    "Area": "Lupane",
    "Chamisa": 13194,
    "Mnangagwa": 14854,
    "Chamisa_percent": 0.43,
    "Mnangagwa_percent": 0.484,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Makonde",
    "Chamisa": 29011,
    "Mnangagwa": 52880,
    "Chamisa_percent": 0.345,
    "Mnangagwa_percent": 0.628,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Makoni",
    "Chamisa": 45764,
    "Mnangagwa": 52328,
    "Chamisa_percent": 0.451,
    "Mnangagwa_percent": 0.515,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Mangwe",
    "Chamisa": 6124,
    "Mnangagwa": 7390,
    "Chamisa_percent": 0.408,
    "Mnangagwa_percent": 0.492,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Marondera",
    "Chamisa": 29485,
    "Mnangagwa": 31546,
    "Chamisa_percent": 0.472,
    "Mnangagwa_percent": 0.505,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Masvingo",
    "Chamisa": 47184,
    "Mnangagwa": 52797,
    "Chamisa_percent": 0.457,
    "Mnangagwa_percent": 0.512,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Matobo",
    "Chamisa": 11447,
    "Mnangagwa": 14169,
    "Chamisa_percent": 0.408,
    "Mnangagwa_percent": 0.505,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Mazowe",
    "Chamisa": 25609,
    "Mnangagwa": 64137,
    "Chamisa_percent": 0.278,
    "Mnangagwa_percent": 0.697,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Mberengwa",
    "Chamisa": 12995,
    "Mnangagwa": 50281,
    "Chamisa_percent": 0.199,
    "Mnangagwa_percent": 0.769,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Mbire",
    "Chamisa": 6476,
    "Mnangagwa": 20322,
    "Chamisa_percent": 0.233,
    "Mnangagwa_percent": 0.73,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Mhondoro-Ngezi",
    "Chamisa": 21055,
    "Mnangagwa": 33429,
    "Chamisa_percent": 0.376,
    "Mnangagwa_percent": 0.597,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Mount Darwin",
    "Chamisa": 13291,
    "Mnangagwa": 77696,
    "Chamisa_percent": 0.139,
    "Mnangagwa_percent": 0.813,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Mudzi",
    "Chamisa": 7102,
    "Mnangagwa": 47950,
    "Chamisa_percent": 0.127,
    "Mnangagwa_percent": 0.855,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Murehwa",
    "Chamisa": 20488,
    "Mnangagwa": 45849,
    "Chamisa_percent": 0.3,
    "Mnangagwa_percent": 0.672,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Mutare",
    "Chamisa": 86758,
    "Mnangagwa": 67009,
    "Chamisa_percent": 0.547,
    "Mnangagwa_percent": 0.422,
    "Winner": "Chamisa"
  },
  {
    "Area": "Mutasa",
    "Chamisa": 44272,
    "Mnangagwa": 30297,
    "Chamisa_percent": 0.577,
    "Mnangagwa_percent": 0.395,
    "Winner": "Chamisa"
  },
  {
    "Area": "Mutoko",
    "Chamisa": 11684,
    "Mnangagwa": 50415,
    "Chamisa_percent": 0.186,
    "Mnangagwa_percent": 0.801,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Muzarabani",
    "Chamisa": 3955,
    "Mnangagwa": 39731,
    "Chamisa_percent": 0.089,
    "Mnangagwa_percent": 0.899,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Mwenezi",
    "Chamisa": 6231,
    "Mnangagwa": 48028,
    "Chamisa_percent": 0.111,
    "Mnangagwa_percent": 0.855,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Nkayi",
    "Chamisa": 11378,
    "Mnangagwa": 16426,
    "Chamisa_percent": 0.313,
    "Mnangagwa_percent": 0.452,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Nyanga",
    "Chamisa": 21907,
    "Mnangagwa": 23887,
    "Chamisa_percent": 0.461,
    "Mnangagwa_percent": 0.502,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Rushinga",
    "Chamisa": 4770,
    "Mnangagwa": 27343,
    "Chamisa_percent": 0.146,
    "Mnangagwa_percent": 0.835,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Sanyati",
    "Chamisa": 29656,
    "Mnangagwa": 46643,
    "Chamisa_percent": 0.379,
    "Mnangagwa_percent": 0.597,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Seke",
    "Chamisa": 20322,
    "Mnangagwa": 16047,
    "Chamisa_percent": 0.544,
    "Mnangagwa_percent": 0.429,
    "Winner": "Chamisa"
  },
  {
    "Area": "Shamva",
    "Chamisa": 8981,
    "Mnangagwa": 45159,
    "Chamisa_percent": 0.163,
    "Mnangagwa_percent": 0.818,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Shurugwi",
    "Chamisa": 15839,
    "Mnangagwa": 28763,
    "Chamisa_percent": 0.347,
    "Mnangagwa_percent": 0.63,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Tsholotsho",
    "Chamisa": 10682,
    "Mnangagwa": 15819,
    "Chamisa_percent": 0.365,
    "Mnangagwa_percent": 0.541,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Umguza",
    "Chamisa": 9650,
    "Mnangagwa": 15798,
    "Chamisa_percent": 0.36,
    "Mnangagwa_percent": 0.59,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "UMP",
    "Chamisa": 3537,
    "Mnangagwa": 44706,
    "Chamisa_percent": 0.073,
    "Mnangagwa_percent": 0.917,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Umzingwane",
    "Chamisa": 9484,
    "Mnangagwa": 10273,
    "Chamisa_percent": 0.452,
    "Mnangagwa_percent": 0.49,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Wedza",
    "Chamisa": 7634,
    "Mnangagwa": 20836,
    "Chamisa_percent": 0.261,
    "Mnangagwa_percent": 0.712,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Zaka",
    "Chamisa": 21148,
    "Mnangagwa": 39560,
    "Chamisa_percent": 0.333,
    "Mnangagwa_percent": 0.623,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Zvimba",
    "Chamisa": 36106,
    "Mnangagwa": 57231,
    "Chamisa_percent": 0.376,
    "Mnangagwa_percent": 0.595,
    "Winner": "Mnangagwa"
  },
  {
    "Area": "Zvishavane",
    "Chamisa": 21500,
    "Mnangagwa": 26703,
    "Chamisa_percent": 0.437,
    "Mnangagwa_percent": 0.543,
    "Winner": "Mnangagwa"
  }
]