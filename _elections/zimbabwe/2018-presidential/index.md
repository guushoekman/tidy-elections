---
layout: election
title: "2018 Zimbabwean presidential election"
country: Zimbabwe
country_code: zw
date: 2018-07-30
wikipedia: https://en.wikipedia.org/wiki/2018_Zimbabwean_general_election
source: https://www.zec.org.zw/pages/election_results2018
maxpercentage: 90
handicap: 1.11
race: both
candidates:
- name: Emmerson Mnangagwa
  name_short: Mnangagwa
  party: ZANU–PF
  color: "#008000"
- name: Nelson Chamisa
  name_short: Chamisa
  party: MDC Alliance
  color: "#DD0000"
---
