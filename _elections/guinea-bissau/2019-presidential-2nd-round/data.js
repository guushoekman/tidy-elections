var data = [
  {
    "Area": "Guinea-Bissau",
    "Pereira": 254468,
    "Embalo": 293359,
    "Pereira_percent": 0.465,
    "Embalo_percent": 0.536,
    "Winner": "Embalo"
  },
  {
    "Area": "Bafatá",
    "Pereira": 23086,
    "Embalo": 51857,
    "Pereira_percent": 0.308,
    "Embalo_percent": 0.692,
    "Winner": "Embalo"
  },
  {
    "Area": "Biombo",
    "Pereira": 22868,
    "Embalo": 14313,
    "Pereira_percent": 0.615,
    "Embalo_percent": 0.385,
    "Winner": "Pereira"
  },
  {
    "Area": "Bissau",
    "Pereira": 93478,
    "Embalo": 62652,
    "Pereira_percent": 0.599,
    "Embalo_percent": 0.401,
    "Winner": "Pereira"
  },
  {
    "Area": "Bolama",
    "Pereira": 8935,
    "Embalo": 3135,
    "Pereira_percent": 0.74,
    "Embalo_percent": 0.26,
    "Winner": "Pereira"
  },
  {
    "Area": "Cacheu",
    "Pereira": 23380,
    "Embalo": 35570,
    "Pereira_percent": 0.397,
    "Embalo_percent": 0.603,
    "Winner": "Embalo"
  },
  {
    "Area": "Gabu",
    "Pereira": 23163,
    "Embalo": 48094,
    "Pereira_percent": 0.325,
    "Embalo_percent": 0.675,
    "Winner": "Embalo"
  },
  {
    "Area": "Oio",
    "Pereira": 34905,
    "Embalo": 43299,
    "Pereira_percent": 0.446,
    "Embalo_percent": 0.554,
    "Winner": "Embalo"
  },
  {
    "Area": "Quinara",
    "Pereira": 9749,
    "Embalo": 13111,
    "Pereira_percent": 0.427,
    "Embalo_percent": 0.574,
    "Winner": "Embalo"
  },
  {
    "Area": "Tombali",
    "Pereira": 10069,
    "Embalo": 18371,
    "Pereira_percent": 0.354,
    "Embalo_percent": 0.646,
    "Winner": "Embalo"
  }
]