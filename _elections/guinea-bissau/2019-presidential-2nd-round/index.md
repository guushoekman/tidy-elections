---
layout: election
title: "2019 Bissau-Guinean presidential election - 2nd round"
country: Guinea-Bissau
country_code: gw
date: 2019-12-29
wikipedia: https://en.wikipedia.org/wiki/2019_Guinea-Bissau_presidential_election
source: http://www.cne.gw/eleicoes/presidencial/resultado-pr
maxpercentage: 75
handicap: 1.33
race: both
candidates:
- name: Domingos Simões Pereira
  name_short: Pereira
  party: PAIGC
  color: "#FF0000"
- name: Umaro Sissoco Embaló
  name_short: Embalo
  party: Madem G15
  color: "#01A35A"
---
