---
layout: election
title: "2019 Bissau-Guinean legislative election"
country: Guinea-Bissau
country_code: gw
date: 2019-03-10
wikipedia: https://en.wikipedia.org/wiki/2019_Guinea-Bissau_legislative_election
source: http://cne.gw/resultados-lg-2019
maxpercentage: 60
handicap: 1.667
race: party
candidates:
- name: PAIGC
  name_short: PAIGC
  party: PAIGC
  color: "#FF0000"
- name: PRS
  name_short: PRS
  party: PRS
  color: "#FFA500"
- name: Madem G15
  name_short: MademG15
  party: Madem G15
  color: "#01A35A"
- name: APU-PDGB
  name_short: APU-PDGB
  party: APU-PDGB
  color: "#094779"
---
