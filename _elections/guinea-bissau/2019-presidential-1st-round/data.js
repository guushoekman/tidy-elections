var data = [
  {
    "Area": "Guinea-Bissau",
    "Pereira": 222870,
    "Nabiam": 73063,
    "Vaz": 68933,
    "Embalo": 153530,
    "Pereira_percent": 0.401,
    "Nabiam_percent": 0.132,
    "Vaz_percent": 0.124,
    "Embalo_percent": 0.277,
    "Winner": "Pereira"
  },
  {
    "Area": "Bafatá",
    "Pereira": 19469,
    "Nabiam": 5407,
    "Vaz": 7304,
    "Embalo": 37671,
    "Pereira_percent": 0.262,
    "Nabiam_percent": 0.073,
    "Vaz_percent": 0.098,
    "Embalo_percent": 0.506,
    "Winner": "Embalo"
  },
  {
    "Area": "Biombo",
    "Pereira": 19691,
    "Nabiam": 4723,
    "Vaz": 4755,
    "Embalo": 5187,
    "Pereira_percent": 0.529,
    "Nabiam_percent": 0.127,
    "Vaz_percent": 0.128,
    "Embalo_percent": 0.139,
    "Winner": "Pereira"
  },
  {
    "Area": "Bissau",
    "Pereira": 87570,
    "Nabiam": 14968,
    "Vaz": 13885,
    "Embalo": 33647,
    "Pereira_percent": 0.553,
    "Nabiam_percent": 0.095,
    "Vaz_percent": 0.088,
    "Embalo_percent": 0.212,
    "Winner": "Pereira"
  },
  {
    "Area": "Bolama",
    "Pereira": 8469,
    "Nabiam": 535,
    "Vaz": 1150,
    "Embalo": 1438,
    "Pereira_percent": 0.687,
    "Nabiam_percent": 0.043,
    "Vaz_percent": 0.093,
    "Embalo_percent": 0.117,
    "Winner": "Pereira"
  },
  {
    "Area": "Cacheu",
    "Pereira": 17231,
    "Nabiam": 12652,
    "Vaz": 19947,
    "Embalo": 7267,
    "Pereira_percent": 0.277,
    "Nabiam_percent": 0.204,
    "Vaz_percent": 0.321,
    "Embalo_percent": 0.117,
    "Winner": "Vaz"
  },
  {
    "Area": "Gabu",
    "Pereira": 20192,
    "Nabiam": 3651,
    "Vaz": 5013,
    "Embalo": 36945,
    "Pereira_percent": 0.285,
    "Nabiam_percent": 0.052,
    "Vaz_percent": 0.071,
    "Embalo_percent": 0.521,
    "Winner": "Embalo"
  },
  {
    "Area": "Oio",
    "Pereira": 28376,
    "Nabiam": 22434,
    "Vaz": 6652,
    "Embalo": 17046,
    "Pereira_percent": 0.357,
    "Nabiam_percent": 0.282,
    "Vaz_percent": 0.084,
    "Embalo_percent": 0.214,
    "Winner": "Pereira"
  },
  {
    "Area": "Quinara",
    "Pereira": 8822,
    "Nabiam": 2872,
    "Vaz": 3049,
    "Embalo": 6807,
    "Pereira_percent": 0.378,
    "Nabiam_percent": 0.123,
    "Vaz_percent": 0.131,
    "Embalo_percent": 0.292,
    "Winner": "Pereira"
  },
  {
    "Area": "Tombali",
    "Pereira": 8288,
    "Nabiam": 5399,
    "Vaz": 5587,
    "Embalo": 6250,
    "Pereira_percent": 0.29,
    "Nabiam_percent": 0.189,
    "Vaz_percent": 0.196,
    "Embalo_percent": 0.219,
    "Winner": "Pereira"
  }
]