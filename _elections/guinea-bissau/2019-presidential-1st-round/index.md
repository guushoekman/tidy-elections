---
layout: election
title: "2019 Bissau-Guinean presidential election - 1st round"
country: Guinea-Bissau
country_code: gw
date: 2019-11-24
wikipedia: https://en.wikipedia.org/wiki/2019_Guinea-Bissau_presidential_election
source: http://www.cne.gw/eleicoes/presidencial/resultado-pr
maxpercentage: 70
handicap: 1.43
race: both
candidates:
- name: Domingos Simões Pereira
  name_short: Pereira
  party: PAIGC
  color: "#FF0000"
- name: Umaro Sissoco Embaló
  name_short: Embalo
  party: Madem G15
  color: "#01A35A"
- name: Nuno Gomes Nabiam
  name_short: Nabiam
  party: APU-PDGB
  color: "#094779"
- name: José Mário Vaz
  name_short: Vaz
  party: Independent
  color: "#333"
---
