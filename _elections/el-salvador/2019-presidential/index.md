---
layout: election
title: "2019 El Salvadorian presidential election"
country: El Salvador
date: 2019-02-03
country_code: sv
wikipedia: https://en.wikipedia.org/wiki/2019_Salvadoran_presidential_election
source: https://www.tse.gob.sv/2019/escrutinio-final/presidencial/index.html
maxpercentage: 60
handicap: 1.667
race: both
candidates:
- name: Nayib Bukele
  name_short: Bukele
  party: GANA
  color: "#4BC0C7"
- name: Carlos Calleja
  name_short: Calleja
  party: ARENA
  color: "#0404B4"
- name: Hugo Martínez
  name_short: Martínez
  party: FMLN
  color: "#F00"
---
