---
layout: election
title: "2019 Indonesian legislative election"
country: Indonesia
country_code: id
date: 2019-04-04
wikipedia: https://en.wikipedia.org/wiki/2019_Indonesian_general_election
source: https://pemilu2019.kpu.go.id
maxpercentage: 60
handicap: 1.67
race: party
candidates:
- name: PDIP
  name_short: PDIP
  party: PDIP
  color: "#D40000"
- name: Gerindra
  name_short: Gerindra
  party: Gerindra
  color: "#B79164"
- name: Golkar
  name_short: Golkar
  party: Golkar
  color: "#FED800"
- name: PKB
  name_short: PKB
  party: PKB
  color: "#00FF00"
- name: NasDem
  name_short: NasDem
  party: NasDem
  color: "#27408B"
- name: PKS
  name_short: PKS
  party: PKS
  color: "#000000"
- name: Demokrat
  name_short: Demokrat
  party: Demokrat
  color: "#2643A3"
- name: PAN
  name_short: PAN
  party: PAN
  color: "#0000FF"
- name: PPP
  name_short: PPP
  party: PPP
  color: "#00A100"
---
