var data = [
  {
    "Area": "Sri Lanka",
    "Rajapaksa": 6924255,
    "Rajapaksa_percent": 0.522,
    "Premadasa": 5564239,
    "Premadasa_percent": 0.42,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Ampara",
    "Rajapaksa": 135058,
    "Rajapaksa_percent": 0.328,
    "Premadasa": 259673,
    "Premadasa_percent": 0.631,
    "Winner": "Premadasa"
  },
  {
    "Area": "Anuradhapura",
    "Rajapaksa": 342223,
    "Rajapaksa_percent": 0.59,
    "Premadasa": 202348,
    "Premadasa_percent": 0.349,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Badulla",
    "Rajapaksa": 276211,
    "Rajapaksa_percent": 0.493,
    "Premadasa": 251706,
    "Premadasa_percent": 0.449,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Batticaloa",
    "Rajapaksa": 38460,
    "Rajapaksa_percent": 0.127,
    "Premadasa": 238649,
    "Premadasa_percent": 0.787,
    "Winner": "Premadasa"
  },
  {
    "Area": "Colombo",
    "Rajapaksa": 727713,
    "Rajapaksa_percent": 0.532,
    "Premadasa": 559921,
    "Premadasa_percent": 0.409,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Galle",
    "Rajapaksa": 466148,
    "Rajapaksa_percent": 0.643,
    "Premadasa": 217401,
    "Premadasa_percent": 0.3,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Gampaha",
    "Rajapaksa": 855870,
    "Rajapaksa_percent": 0.593,
    "Premadasa": 494671,
    "Premadasa_percent": 0.343,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Hambantota",
    "Rajapaksa": 278804,
    "Rajapaksa_percent": 0.662,
    "Premadasa": 108906,
    "Premadasa_percent": 0.258,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Jaffna",
    "Rajapaksa": 23261,
    "Rajapaksa_percent": 0.062,
    "Premadasa": 312722,
    "Premadasa_percent": 0.839,
    "Winner": "Premadasa"
  },
  {
    "Area": "Kalutara",
    "Rajapaksa": 482920,
    "Rajapaksa_percent": 0.595,
    "Premadasa": 284213,
    "Premadasa_percent": 0.35,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Kandy",
    "Rajapaksa": 471502,
    "Rajapaksa_percent": 0.504,
    "Premadasa": 417355,
    "Premadasa_percent": 0.446,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Kegalle",
    "Rajapaksa": 320484,
    "Rajapaksa_percent": 0.557,
    "Premadasa": 228032,
    "Premadasa_percent": 0.396,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Kurunegala",
    "Rajapaksa": 652278,
    "Rajapaksa_percent": 0.579,
    "Premadasa": 416961,
    "Premadasa_percent": 0.37,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Matale",
    "Rajapaksa": 187821,
    "Rajapaksa_percent": 0.554,
    "Premadasa": 134291,
    "Premadasa_percent": 0.396,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Matara",
    "Rajapaksa": 374481,
    "Rajapaksa_percent": 0.672,
    "Premadasa": 149026,
    "Premadasa_percent": 0.268,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Monaragala",
    "Rajapaksa": 208814,
    "Rajapaksa_percent": 0.653,
    "Premadasa": 92539,
    "Premadasa_percent": 0.29,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Nuwara Eliya",
    "Rajapaksa": 175823,
    "Rajapaksa_percent": 0.369,
    "Premadasa": 277913,
    "Premadasa_percent": 0.583,
    "Winner": "Premadasa"
  },
  {
    "Area": "Polonnaruwa",
    "Rajapaksa": 147340,
    "Rajapaksa_percent": 0.53,
    "Premadasa": 112473,
    "Premadasa_percent": 0.405,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Puttalam",
    "Rajapaksa": 230760,
    "Rajapaksa_percent": 0.508,
    "Premadasa": 199356,
    "Premadasa_percent": 0.439,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Ratnapura",
    "Rajapaksa": 448044,
    "Rajapaksa_percent": 0.599,
    "Premadasa": 264503,
    "Premadasa_percent": 0.354,
    "Winner": "Rajapaksa"
  },
  {
    "Area": "Trincomalee",
    "Rajapaksa": 54135,
    "Rajapaksa_percent": 0.234,
    "Premadasa": 166841,
    "Premadasa_percent": 0.721,
    "Winner": "Premadasa"
  },
  {
    "Area": "Vanni",
    "Rajapaksa": 26105,
    "Rajapaksa_percent": 0.123,
    "Premadasa": 174739,
    "Premadasa_percent": 0.821,
    "Winner": "Premadasa"
  }
]