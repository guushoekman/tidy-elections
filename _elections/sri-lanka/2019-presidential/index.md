---
layout: election
title: "2019 Sri Lankan presidential election"
country: Sri Lanka
date: 2019-11-16
country_code: lk
wikipedia: https://en.wikipedia.org/wiki/2019_Sri_Lankan_presidential_election
source: https://results.elections.gov.lk/
maxpercentage: 85
handicap: 1.175
race: both
candidates:
- name: Gotabaya Rajapaksa
  name_short: Rajapaksa
  party: SLPP
  color: "#9D1B25"
- name: Sajith Premadasa
  name_short: Premadasa
  party: UNP
  color: "#1CAE05"
---
