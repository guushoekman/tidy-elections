---
layout: election
title: "2019 Malawian presidential election"
country: Malawi
country_code: mw
date: 2019-05-21
wikipedia: https://en.wikipedia.org/wiki/2019_Malawian_general_election
source: https://www.mec.org.mw/elections/
maxpercentage: 100
handicap: 1
race: both
candidates:
- name: Peter Mutharika
  name_short: DPP
  party: DPP
  color: "#1E90FF"
- name: Lazarus Chakwera
  name_short: MCP
  party: MCP
  color: "#228b22"
- name: Saulos Chilima
  name_short: UTM
  party: UTM
  color: "#DC143C"
---
