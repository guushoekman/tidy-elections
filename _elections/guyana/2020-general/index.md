---
layout: election
title: "2020 Guyanese general election"
country: Guyana
country_code: gy
date: 2020-03-02
wikipedia: https://en.wikipedia.org/wiki/2020_Guyanese_general_election
source: https://www.gecom.org.gy/home/
maxpercentage: 85
handicap: 1.175
race: both
candidates:
- name: "David A. Granger"
  name_short: APNU-AFC
  party: APNU-AFC
  color: "#24BB04"
- name: "Irfaan Ali"
  name_short: PPP-C
  party: PPP-C
  color: "#BF1B2C"
---
