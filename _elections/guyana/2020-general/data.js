var data = [
  {
    "Area": "Guyana",
    "APNU-AFC": 236928,
    "PPP-C": 229481,
    "APNU-AFC_percent": 0.498,
    "PPP-C_percent": 0.483,
    "Winner": "APNU-AFC"
  },
  {
    "Area": "Barima-Waini",
    "APNU-AFC": 3905,
    "PPP-C": 8022,
    "APNU-AFC_percent": 0.322,
    "PPP-C_percent": 0.661,
    "Winner": "PPP-C"
  },
  {
    "Area": "Pomeroon-Supenaam",
    "APNU-AFC": 7343,
    "PPP-C": 18788,
    "APNU-AFC_percent": 0.276,
    "PPP-C_percent": 0.706,
    "Winner": "PPP-C"
  },
  {
    "Area": "Essequibo Islands-West Demerara",
    "APNU-AFC": 23811,
    "PPP-C": 47855,
    "APNU-AFC_percent": 0.328,
    "PPP-C_percent": 0.659,
    "Winner": "PPP-C"
  },
  {
    "Area": "Demerara-Mahaica",
    "APNU-AFC": 136057,
    "PPP-C": 77231,
    "APNU-AFC_percent": 0.626,
    "PPP-C_percent": 0.355,
    "Winner": "APNU-AFC"
  },
  {
    "Area": "Mahaica-Berbice",
    "APNU-AFC": 14497,
    "PPP-C": 18317,
    "APNU-AFC_percent": 0.438,
    "PPP-C_percent": 0.553,
    "Winner": "PPP-C"
  },
  {
    "Area": "East Berbice-Corentyne",
    "APNU-AFC": 20338,
    "PPP-C": 43275,
    "APNU-AFC_percent": 0.316,
    "PPP-C_percent": 0.672,
    "Winner": "PPP-C"
  },
  {
    "Area": "Cuyuni-Mazaruni",
    "APNU-AFC": 4817,
    "PPP-C": 3720,
    "APNU-AFC_percent": 0.503,
    "PPP-C_percent": 0.388,
    "Winner": "APNU-AFC"
  },
  {
    "Area": "Potaro-Siparuni",
    "APNU-AFC": 2086,
    "PPP-C": 2041,
    "APNU-AFC_percent": 0.451,
    "PPP-C_percent": 0.441,
    "Winner": "APNU-AFC"
  },
  {
    "Area": "Upper Takutu-Upper Essequibo",
    "APNU-AFC": 4889,
    "PPP-C": 7068,
    "APNU-AFC_percent": 0.399,
    "PPP-C_percent": 0.576,
    "Winner": "PPP-C"
  },
  {
    "Area": "Upper Demerara-Berbice",
    "APNU-AFC": 19185,
    "PPP-C": 3164,
    "APNU-AFC_percent": 0.842,
    "PPP-C_percent": 0.139,
    "Winner": "APNU-AFC"
  }
]