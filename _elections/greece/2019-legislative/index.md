---
layout: election
title: "2019 Greek legislative election"
country: Greece
date: 2019-07-07
country_code: gr
wikipedia: https://en.wikipedia.org/wiki/2019_Greek_legislative_election
source: https://ekloges.ypes.gr/current/v/home/en/
maxpercentage: 50
handicap: 2
race: party
candidates:
- name: New Democracy
  name_short: ND
  party: New Democracy
  color: "#166BC7"
- name: Syriza
  name_short: SYRIZA
  party: Syriza
  color: "#FA8072"
- name: Movement for Change
  name_short: KINAL
  party: Movement for Change
  color: "#019547"
- name: Communist Party of Greece
  name_short: KKE
  party: Communist Party of Greece
  color: "#C80000"
- name: Greek Solution
  name_short: EL
  party: Greek Solution
  color: "#80DAEB"
- name: MeRA25
  name_short: MeRA25
  party: MeRA25
  color: "#EF3F1E"
---
