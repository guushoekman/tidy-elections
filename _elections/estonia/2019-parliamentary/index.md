---
layout: election
title: "2019 Estonian parliamentary election"
country: Estonia
country_code: ee
date: 2019-03-03
wikipedia: https://en.wikipedia.org/wiki/2019_Estonian_parliamentary_election
source: https://rk2019.valimised.ee/en/election-result/election-result.html
maxpercentage: 50
handicap: 2
race: party
candidates:
- name: REF
  name_short: REF
  party: REF
  color: "#FFDC00"
- name: Kesk
  name_short: Kesk
  party: Kesk
  color: "#007D5A"
- name: EKRE
  name_short: EKRE
  party: EKRE
  color: "#0077BD"
- name: Isamaa
  name_short: Isamaa
  party: Isamaa
  color: "#009DE0"
- name: SDE
  name_short: SDE
  party: SDE
  color: "#E10600"
---
