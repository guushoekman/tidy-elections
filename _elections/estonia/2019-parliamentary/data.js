var data = [
  {
    "Area": "Estonia",
    "REF": 162363,
    "Kesk": 129618,
    "EKRE": 99671,
    "Isamaa": 64219,
    "SDE": 55175,
    "REF_percent": 0.289,
    "Kesk_percent": 0.231,
    "EKRE_percent": 0.178,
    "Isamaa_percent": 0.114,
    "SDE_percent": 0.098,
    "Winner": "Kesk"
  },
  {
    "Area": "Tartu",
    "REF": 15504,
    "Kesk": 6099,
    "EKRE": 7641,
    "Isamaa": 5416,
    "SDE": 5087,
    "REF_percent": 0.346,
    "Kesk_percent": 0.136,
    "EKRE_percent": 0.17,
    "Isamaa_percent": 0.121,
    "SDE_percent": 0.113,
    "Winner": "REF"
  },
  {
    "Area": "Jõgeva and Tartu",
    "REF": 11399,
    "Kesk": 6438,
    "EKRE": 9362,
    "Isamaa": 7308,
    "SDE": 3891,
    "REF_percent": 0.272,
    "Kesk_percent": 0.154,
    "EKRE_percent": 0.224,
    "Isamaa_percent": 0.175,
    "SDE_percent": 0.093,
    "Winner": "REF"
  },
  {
    "Area": "Järva and Viljandi",
    "REF": 9777,
    "Kesk": 6253,
    "EKRE": 8389,
    "Isamaa": 6176,
    "SDE": 4682,
    "REF_percent": 0.261,
    "Kesk_percent": 0.167,
    "EKRE_percent": 0.224,
    "Isamaa_percent": 0.165,
    "SDE_percent": 0.125,
    "Winner": "REF"
  },
  {
    "Area": "Pärnu",
    "REF": 10860,
    "Kesk": 7913,
    "EKRE": 11575,
    "Isamaa": 5045,
    "SDE": 2780,
    "REF_percent": 0.263,
    "Kesk_percent": 0.192,
    "EKRE_percent": 0.281,
    "Isamaa_percent": 0.122,
    "SDE_percent": 0.067,
    "Winner": "EKRE"
  },
  {
    "Area": "Harju and Rapla",
    "REF": 34571,
    "Kesk": 13681,
    "EKRE": 16638,
    "Isamaa": 10094,
    "SDE": 7621,
    "REF_percent": 0.381,
    "Kesk_percent": 0.151,
    "EKRE_percent": 0.183,
    "Isamaa_percent": 0.111,
    "SDE_percent": 0.084,
    "Winner": "REF"
  },
  {
    "Area": "Hiiu, Lääne and Saare",
    "REF": 9150,
    "Kesk": 5736,
    "EKRE": 6894,
    "Isamaa": 3347,
    "SDE": 3414,
    "REF_percent": 0.286,
    "Kesk_percent": 0.179,
    "EKRE_percent": 0.215,
    "Isamaa_percent": 0.105,
    "SDE_percent": 0.107,
    "Winner": "REF"
  },
  {
    "Area": "Haabersti, Põhja-Tallinn and Kristiine",
    "REF": 16299,
    "Kesk": 16950,
    "EKRE": 6361,
    "Isamaa": 4950,
    "SDE": 5683,
    "REF_percent": 0.29,
    "Kesk_percent": 0.302,
    "EKRE_percent": 0.113,
    "Isamaa_percent": 0.088,
    "SDE_percent": 0.101,
    "Winner": "Kesk"
  },
  {
    "Area": "Mustamäe and Nõmme",
    "REF": 16209,
    "Kesk": 11945,
    "EKRE": 7055,
    "Isamaa": 5086,
    "SDE": 4651,
    "REF_percent": 0.325,
    "Kesk_percent": 0.239,
    "EKRE_percent": 0.141,
    "Isamaa_percent": 0.102,
    "SDE_percent": 0.093,
    "Winner": "REF"
  },
  {
    "Area": "Kesklinn, Lasnamäe and Pirita",
    "REF": 18400,
    "Kesk": 26335,
    "EKRE": 7110,
    "Isamaa": 5416,
    "SDE": 5896,
    "REF_percent": 0.263,
    "Kesk_percent": 0.376,
    "EKRE_percent": 0.102,
    "Isamaa_percent": 0.077,
    "SDE_percent": 0.084,
    "Winner": "Kesk"
  },
  {
    "Area": "Võru, Valga and Põlva",
    "REF": 10029,
    "Kesk": 9197,
    "EKRE": 10644,
    "Isamaa": 4153,
    "SDE": 5034,
    "REF_percent": 0.233,
    "Kesk_percent": 0.213,
    "EKRE_percent": 0.247,
    "Isamaa_percent": 0.096,
    "SDE_percent": 0.117,
    "Winner": "EKRE"
  },
  {
    "Area": "Lääne-Viru",
    "REF": 6396,
    "Kesk": 5371,
    "EKRE": 5768,
    "Isamaa": 5461,
    "SDE": 2428,
    "REF_percent": 0.237,
    "Kesk_percent": 0.199,
    "EKRE_percent": 0.214,
    "Isamaa_percent": 0.203,
    "SDE_percent": 0.09,
    "Winner": "REF"
  },
  {
    "Area": "Ida-Viru",
    "REF": 3769,
    "Kesk": 13700,
    "EKRE": 2234,
    "Isamaa": 1767,
    "SDE": 4008,
    "REF_percent": 0.14,
    "Kesk_percent": 0.507,
    "EKRE_percent": 0.083,
    "Isamaa_percent": 0.065,
    "SDE_percent": 0.148,
    "Winner": "Kesk"
  }
]