---
layout: election
title: "2018 Costa Rican presidential election - 2nd round"
country: Costa Rica
country_code: cr
date: 2018-04-01
wikipedia: https://en.wikipedia.org/wiki/2018_Costa_Rican_general_election
source: http://resultados2018.tse.go.cr/resultados2darondadefinitivos/#/presidenciales
maxpercentage: 75
handicap: 1.333
race: both
candidates:
- name: Carlos Alvarado
  name_short: PAC
  party: PAC
  color: "#0059CF"
- name: Fabricio Alvarado
  name_short: PREN
  party: PREN
  color: "#FFD700"
---
