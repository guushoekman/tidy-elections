---
layout: election
title: "2018 Timorese parliamentary election"
country: Timor Leste
country_code: tl
date: 2018-05-12
wikipedia: https://en.wikipedia.org/wiki/2018_East_Timorese_parliamentary_election
source: http://www.cne.tl/apuramento.2018/public.php
maxpercentage: 65
handicap: 1.538
race: party
candidates:
- name: AMP
  name_short: AMP
  party: AMP
  color: "#10B0E0"
- name: FRETILIN
  name_short: FRETILIN
  party: FRETILIN
  color: "#F00"
- name: PD
  name_short: PD
  party: PD
  color: "#004080"
- name: FDD
  name_short: FDD
  party: FDD
  color: "#F0F040"
---
