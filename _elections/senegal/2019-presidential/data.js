var data = [
  {
    "Area": "Senegal",
    "Sall": 2555426,
    "Seck": 899556,
    "Sonko": 687523,
    "Sall_percent": 0.583,
    "Seck_percent": 0.205,
    "Sonko_percent": 0.157,
    "Winner": "Sall"
  },
  {
    "Area": "Bakel",
    "Sall": 25723,
    "Seck": 2908,
    "Sonko": 4888,
    "Sall_percent": 0.748,
    "Seck_percent": 0.085,
    "Sonko_percent": 0.142,
    "Winner": "Sall"
  },
  {
    "Area": "Bambey",
    "Sall": 38625,
    "Seck": 25364,
    "Sonko": 4679,
    "Sall_percent": 0.541,
    "Seck_percent": 0.356,
    "Sonko_percent": 0.066,
    "Winner": "Sall"
  },
  {
    "Area": "Bignona",
    "Sall": 27398,
    "Seck": 2169,
    "Sonko": 51438,
    "Sall_percent": 0.335,
    "Seck_percent": 0.027,
    "Sonko_percent": 0.629,
    "Winner": "Sonko"
  },
  {
    "Area": "Birkilane",
    "Sall": 22854,
    "Seck": 3309,
    "Sonko": 3392,
    "Sall_percent": 0.733,
    "Seck_percent": 0.106,
    "Sonko_percent": 0.109,
    "Winner": "Sall"
  },
  {
    "Area": "Bounkiling",
    "Sall": 23385,
    "Seck": 1105,
    "Sonko": 12605,
    "Sall_percent": 0.617,
    "Seck_percent": 0.029,
    "Sonko_percent": 0.333,
    "Winner": "Sall"
  },
  {
    "Area": "Dagana",
    "Sall": 62091,
    "Seck": 11033,
    "Sonko": 11847,
    "Sall_percent": 0.673,
    "Seck_percent": 0.12,
    "Sonko_percent": 0.128,
    "Winner": "Sall"
  },
  {
    "Area": "Dakar",
    "Sall": 212355,
    "Seck": 115612,
    "Sonko": 101003,
    "Sall_percent": 0.468,
    "Seck_percent": 0.255,
    "Sonko_percent": 0.223,
    "Winner": "Sall"
  },
  {
    "Area": "Diourbel",
    "Sall": 40501,
    "Seck": 22026,
    "Sonko": 6300,
    "Sall_percent": 0.567,
    "Seck_percent": 0.309,
    "Sonko_percent": 0.088,
    "Winner": "Sall"
  },
  {
    "Area": "Fatick",
    "Sall": 80888,
    "Seck": 5172,
    "Sonko": 9349,
    "Sall_percent": 0.807,
    "Seck_percent": 0.052,
    "Sonko_percent": 0.093,
    "Winner": "Sall"
  },
  {
    "Area": "Foundiougne",
    "Sall": 62373,
    "Seck": 5103,
    "Sonko": 7905,
    "Sall_percent": 0.807,
    "Seck_percent": 0.066,
    "Sonko_percent": 0.102,
    "Winner": "Sall"
  },
  {
    "Area": "Gossas",
    "Sall": 20292,
    "Seck": 4299,
    "Sonko": 2036,
    "Sall_percent": 0.736,
    "Seck_percent": 0.156,
    "Sonko_percent": 0.074,
    "Winner": "Sall"
  },
  {
    "Area": "Goudiry",
    "Sall": 21641,
    "Seck": 1964,
    "Sonko": 3514,
    "Sall_percent": 0.769,
    "Seck_percent": 0.07,
    "Sonko_percent": 0.125,
    "Winner": "Sall"
  },
  {
    "Area": "Goudomp",
    "Sall": 23343,
    "Seck": 4434,
    "Sonko": 17012,
    "Sall_percent": 0.505,
    "Seck_percent": 0.096,
    "Sonko_percent": 0.368,
    "Winner": "Sall"
  },
  {
    "Area": "Guediawaye",
    "Sall": 65174,
    "Seck": 35646,
    "Sonko": 24243,
    "Sall_percent": 0.488,
    "Seck_percent": 0.267,
    "Sonko_percent": 0.181,
    "Winner": "Sall"
  },
  {
    "Area": "Guinguineo",
    "Sall": 21723,
    "Seck": 5786,
    "Sonko": 2964,
    "Sall_percent": 0.645,
    "Seck_percent": 0.172,
    "Sonko_percent": 0.088,
    "Winner": "Sall"
  },
  {
    "Area": "Kaffrine",
    "Sall": 38874,
    "Seck": 6513,
    "Sonko": 5294,
    "Sall_percent": 0.709,
    "Seck_percent": 0.119,
    "Sonko_percent": 0.097,
    "Winner": "Sall"
  },
  {
    "Area": "Kanel",
    "Sall": 65679,
    "Seck": 1364,
    "Sonko": 2562,
    "Sall_percent": 0.937,
    "Seck_percent": 0.019,
    "Sonko_percent": 0.037,
    "Winner": "Sall"
  },
  {
    "Area": "Kaolack",
    "Sall": 99376,
    "Seck": 18649,
    "Sonko": 22677,
    "Sall_percent": 0.677,
    "Seck_percent": 0.127,
    "Sonko_percent": 0.154,
    "Winner": "Sall"
  },
  {
    "Area": "Kebemer",
    "Sall": 42166,
    "Seck": 26739,
    "Sonko": 5125,
    "Sall_percent": 0.524,
    "Seck_percent": 0.332,
    "Sonko_percent": 0.064,
    "Winner": "Sall"
  },
  {
    "Area": "Kedougou",
    "Sall": 16153,
    "Seck": 2494,
    "Sonko": 3269,
    "Sall_percent": 0.714,
    "Seck_percent": 0.11,
    "Sonko_percent": 0.145,
    "Winner": "Sall"
  },
  {
    "Area": "Kolda",
    "Sall": 36927,
    "Seck": 6245,
    "Sonko": 13839,
    "Sall_percent": 0.628,
    "Seck_percent": 0.106,
    "Sonko_percent": 0.235,
    "Winner": "Sall"
  },
  {
    "Area": "Koumpentoum",
    "Sall": 20220,
    "Seck": 4613,
    "Sonko": 3652,
    "Sall_percent": 0.684,
    "Seck_percent": 0.156,
    "Sonko_percent": 0.123,
    "Winner": "Sall"
  },
  {
    "Area": "Koungheul",
    "Sall": 33728,
    "Seck": 5020,
    "Sonko": 2848,
    "Sall_percent": 0.786,
    "Seck_percent": 0.117,
    "Sonko_percent": 0.066,
    "Winner": "Sall"
  },
  {
    "Area": "Linguere",
    "Sall": 64291,
    "Seck": 5857,
    "Sonko": 5666,
    "Sall_percent": 0.811,
    "Seck_percent": 0.074,
    "Sonko_percent": 0.071,
    "Winner": "Sall"
  },
  {
    "Area": "Louga",
    "Sall": 73514,
    "Seck": 19788,
    "Sonko": 9612,
    "Sall_percent": 0.608,
    "Seck_percent": 0.164,
    "Sonko_percent": 0.08,
    "Winner": "Sall"
  },
  {
    "Area": "Malem Hodar",
    "Sall": 18926,
    "Seck": 2582,
    "Sonko": 1285,
    "Sall_percent": 0.771,
    "Seck_percent": 0.105,
    "Sonko_percent": 0.052,
    "Winner": "Sall"
  },
  {
    "Area": "Matam",
    "Sall": 92454,
    "Seck": 2239,
    "Sonko": 3698,
    "Sall_percent": 0.931,
    "Seck_percent": 0.023,
    "Sonko_percent": 0.037,
    "Winner": "Sall"
  },
  {
    "Area": "Mbacke",
    "Sall": 67745,
    "Seck": 129724,
    "Sonko": 12686,
    "Sall_percent": 0.304,
    "Seck_percent": 0.583,
    "Sonko_percent": 0.057,
    "Winner": "Seck"
  },
  {
    "Area": "Mbour",
    "Sall": 132202,
    "Seck": 35437,
    "Sonko": 29896,
    "Sall_percent": 0.626,
    "Seck_percent": 0.168,
    "Sonko_percent": 0.142,
    "Winner": "Sall"
  },
  {
    "Area": "Medina Yoro Foulah",
    "Sall": 19992,
    "Seck": 2888,
    "Sonko": 4133,
    "Sall_percent": 0.721,
    "Seck_percent": 0.104,
    "Sonko_percent": 0.149,
    "Winner": "Sall"
  },
  {
    "Area": "Nioro du Rip",
    "Sall": 72463,
    "Seck": 7180,
    "Sonko": 9825,
    "Sall_percent": 0.786,
    "Seck_percent": 0.078,
    "Sonko_percent": 0.107,
    "Winner": "Sall"
  },
  {
    "Area": "Oussouye",
    "Sall": 8707,
    "Seck": 406,
    "Sonko": 9209,
    "Sall_percent": 0.467,
    "Seck_percent": 0.022,
    "Sonko_percent": 0.494,
    "Winner": "Sonko"
  },
  {
    "Area": "Pikine",
    "Sall": 183630,
    "Seck": 90388,
    "Sonko": 74255,
    "Sall_percent": 0.486,
    "Seck_percent": 0.239,
    "Sonko_percent": 0.197,
    "Winner": "Sall"
  },
  {
    "Area": "Podor",
    "Sall": 127639,
    "Seck": 2806,
    "Sonko": 4802,
    "Sall_percent": 0.934,
    "Seck_percent": 0.021,
    "Sonko_percent": 0.035,
    "Winner": "Sall"
  },
  {
    "Area": "Ranerou",
    "Sall": 14827,
    "Seck": 528,
    "Sonko": 482,
    "Sall_percent": 0.921,
    "Seck_percent": 0.033,
    "Sonko_percent": 0.03,
    "Winner": "Sall"
  },
  {
    "Area": "Rufisque",
    "Sall": 100632,
    "Seck": 34892,
    "Sonko": 30186,
    "Sall_percent": 0.546,
    "Seck_percent": 0.189,
    "Sonko_percent": 0.164,
    "Winner": "Sall"
  },
  {
    "Area": "Saint-Louis",
    "Sall": 62788,
    "Seck": 17404,
    "Sonko": 18632,
    "Sall_percent": 0.574,
    "Seck_percent": 0.159,
    "Sonko_percent": 0.17,
    "Winner": "Sall"
  },
  {
    "Area": "Salemata",
    "Sall": 4525,
    "Seck": 1062,
    "Sonko": 589,
    "Sall_percent": 0.69,
    "Seck_percent": 0.162,
    "Sonko_percent": 0.09,
    "Winner": "Sall"
  },
  {
    "Area": "Saraya",
    "Sall": 7122,
    "Seck": 1383,
    "Sonko": 2082,
    "Sall_percent": 0.642,
    "Seck_percent": 0.125,
    "Sonko_percent": 0.188,
    "Winner": "Sall"
  },
  {
    "Area": "Sedhiou",
    "Sall": 23414,
    "Seck": 3808,
    "Sonko": 14833,
    "Sall_percent": 0.543,
    "Seck_percent": 0.088,
    "Sonko_percent": 0.344,
    "Winner": "Sall"
  },
  {
    "Area": "Tambacounda",
    "Sall": 46096,
    "Seck": 5401,
    "Sonko": 11258,
    "Sall_percent": 0.709,
    "Seck_percent": 0.083,
    "Sonko_percent": 0.173,
    "Winner": "Sall"
  },
  {
    "Area": "Thies",
    "Sall": 100422,
    "Seck": 120054,
    "Sonko": 19737,
    "Sall_percent": 0.394,
    "Seck_percent": 0.471,
    "Sonko_percent": 0.078,
    "Winner": "Seck"
  },
  {
    "Area": "Tivaouane",
    "Sall": 79757,
    "Seck": 55966,
    "Sonko": 9836,
    "Sall_percent": 0.493,
    "Seck_percent": 0.346,
    "Sonko_percent": 0.061,
    "Winner": "Sall"
  },
  {
    "Area": "Velingara",
    "Sall": 47551,
    "Seck": 8106,
    "Sonko": 10423,
    "Sall_percent": 0.695,
    "Seck_percent": 0.118,
    "Sonko_percent": 0.152,
    "Winner": "Sall"
  },
  {
    "Area": "Ziguinchor",
    "Sall": 32846,
    "Seck": 2419,
    "Sonko": 41291,
    "Sall_percent": 0.423,
    "Seck_percent": 0.031,
    "Sonko_percent": 0.532,
    "Winner": "Sonko"
  }
]