# Contribute to Tidy Elections

There are many ways you can help contribute to Tidy Elections.

## Election results

Probably the most obvious way to contribute is by providing election result data. This can be in various forms:

- A link to the data
- The raw data from the original source
- A cleaned spreadsheet
- The finished product in JSON format (like [this](https://gitlab.com/guushoekman/tidy-elections/blob/master/_elections/el-salvador/2019-presidential/data.js))

Or anything in between. If you have any election results that are reasonably difficult to find and/or on the [wishlist](https://tidyelections.in/elections.html), please share them. You can [email them](mailto:mail@guushoekman.com), [open an issue](https://gitlab.com/guushoekman/tidy-elections/issues) or do a [merge request](https://gitlab.com/guushoekman/tidy-elections/merge_requests).

## Basemaps

Each election has an SVG basemap that's used to visualise the data. Having more of these is extremely welcome.

It's important that each subnational area has a `title` attribute with the name of the area. Check out [the map of Malawi](https://gitlab.com/guushoekman/tidy-elections/blob/master/_elections/malawi/2019-presidential/map.svg) for an example.

They're pretty straight forward. The biggest issue is finding versions that include the names. Failing that, the biggest hurdle is the rather boring task of adding all the names manually.

As an FYI: there are ways to [convert shapefiles, geoJSON files, and most other formats into SVG](https://gis.stackexchange.com/a/323182). If an SVG version can't be easily found, [Natural Earth](https://www.naturalearthdata.com/downloads/) is an extremely useful backup.

## Adding features

Tidy Elections can certainly benefit from a few extra features. Any help with these would be highly appreciated.

* [ ] Make it possible for an interested party (most likely a news organisation) to easily embed an election on their website.
* [ ] Reduce CSS and JS bloat. Tidy Elections loads the entire CSS and JS of [Fomantic UI](https://fomantic-ui.com/) but only uses a fraction of it. Whatever isn't used shouldn't be loaded.
* [ ] The legends are generated once and the ranges are the same for all the parties. It doesn't make sense that two parties with vastly different results use the same legend range.
* [ ] It might be nice to be able to toggle a country's major cities to add context to the results.

If you know of other ways Tidy Elections can improve please [open an issue](https://gitlab.com/guushoekman/tidy-elections/issues).

## Verifying data

There is no inherent reason why anyone should trust the data on Tidy Elections. In fact, it's not unlikely that there are a few mistakes here and there. Having people verify the data and correct any mistakes is enormously helpful and would increase the legitimacy of the data and Tidy Elections as a whole.

Each election has a CSV file it uses as a data source. You can download these and compare them to the official source of the election results. You can find links to both of these on each election page.

Besides the data, you can also verify the maps. These are gathered from various sources and largely assumed to be correct, but without intimate knowledge of a country's subnational regions it's extremely difficult to know if the boundaries are correct. They don't need to be 100% accurate, but shouldn't have any glaring errors.